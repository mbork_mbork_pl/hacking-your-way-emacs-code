(defun move-line-down (count)
  "Move the current line down."
  (interactive "*p")
  (unless (zerop count)
    (save-excursion
      (push (point) buffer-undo-list)
      (forward-line (if (> count 0) 1 0))
      (let ((region-to-move-up
	     (delete-and-extract-region
	      (point)
	      (save-excursion
		(forward-line count)
		(point)))))
	(forward-line (if (> count 0) -1 1))
	(insert-before-markers region-to-move-up)
	(indent-according-to-mode)))))

(defun move-line-up (count)
  "Move the current line up."
  (interactive "*p")
  (move-line-down (- count)))
